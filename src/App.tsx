import React, { createContext, FC, useContext, useMemo, useState } from 'react';
import Home from './home/Home';
import Experiences from './experiences/Experiences';
import {
  Fab,
  Paper,
  Theme,
  ThemeProvider,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import { darkTheme, lightTheme } from './themes';
import { DarkModeRounded, LightModeRounded } from '@mui/icons-material';
import Education from './education/Education';
import Languages from './languages/Languages';
import Competences from './competences/Competences';
import Footer from './footer/Footer';

const ColorModeContext = createContext({ toggleColorMode: () => {} });

const ThemeToggle: FC = () => {
  const theme = useTheme();
  const colorMode = useContext(ColorModeContext);
  return (
    <Fab
      color="primary"
      style={{
        position: 'fixed',
        bottom: theme.spacing(5),
        right: theme.spacing(5),
      }}
      onClick={colorMode.toggleColorMode}
    >
      {theme.palette.mode === 'light' ? (
        <DarkModeRounded />
      ) : (
        <LightModeRounded />
      )}
    </Fab>
  );
};

const App: FC = () => {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)', {
    noSsr: true, // Doesn't work on firefox without this
  });
  const [theme, setTheme] = useState<Theme>(
    prefersDarkMode ? darkTheme : lightTheme,
  );
  const colorMode = useMemo(
    () => ({
      toggleColorMode: () =>
        setTheme(pt => (pt == lightTheme ? darkTheme : lightTheme)),
    }),
    [],
  );
  const separator = <div style={{ height: theme.spacing(20) }} />;
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <Paper square>
          <Home />
          {separator}
          <Experiences />
          {separator}
          <Education />
          {separator}
          <Competences />
          {separator}
          <Languages />
          {separator}
          <Footer />
        </Paper>
        <ThemeToggle />
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};

export default App;
