import React, { FC, useCallback, useEffect, useState } from 'react';
import {
  Alert,
  Autocomplete,
  Container,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  Paper,
  TextField,
  Typography,
  useTheme,
} from '@mui/material';
import { Profile, profiles } from './data';
import { allIcons, Icon } from '../shared/icons/Icons';
import Fuse from 'fuse.js';
import { Element } from 'react-scroll';
import { Ids } from '../ids';

const fuseOptions = {
  includeScore: true,
  shouldSort: true,
  keys: ['name'],
};
const fuseBuilder = (data: ReadonlyArray<Icon>) => new Fuse(data, fuseOptions);

interface State {
  search: Array<Profile | string>;
  list: Icon[];
}

const Competences: FC = () => {
  const [{ search, list }, setState] = useState<State>({
    search: [],
    list: allIcons,
  });
  const toggleProfile = useCallback(
    (p: Profile) =>
      setState(ps => ({
        ...ps,
        search: ps.search.includes(p)
          ? ps.search.filter(pr => pr !== p)
          : [...ps.search, p],
      })),
    [setState],
  );
  useEffect(() => {
    const tags = (search.filter(v => typeof v !== 'string') as Profile[]).map(
      v => v.tag,
    );
    let icons =
      tags.length === 0
        ? allIcons
        : allIcons.filter(i => i.tags.some(t => tags.includes(t)));
    const strings = search.filter(v => typeof v === 'string') as string[];
    icons =
      strings.length === 0
        ? icons
        : fuseBuilder(icons)
            .search(strings.join(' '))
            .map(r => r.item);
    setState(ps => ({ ...ps, list: icons }));
  }, [search]);
  const borderRadius = useTheme().shape.borderRadius;
  return (
    <Element name={Ids.competences}>
      <Container>
        <Grid container direction="column" spacing={10} alignItems="stretch">
          <Grid item>
            <Typography variant="h2" textAlign="center">
              Competences
            </Typography>
          </Grid>
          <Grid item container spacing={10}>
            <Grid item lg={6} xs={12} container direction="column" spacing={2}>
              <Grid item>
                <Typography variant="h4" textAlign="center">
                  Profiles
                </Typography>
                <Typography sx={{ display: { lg: 'none' } }} textAlign="center">
                  (See the corresponding skills below after selection)
                </Typography>
              </Grid>
              <Grid item>
                <Paper variant="outlined">
                  <List sx={{ padding: 0 }}>
                    {profiles.map((p, i) => {
                      let style = undefined;
                      if (i === 0) {
                        style = {
                          borderTopLeftRadius: borderRadius,
                          borderTopRightRadius: borderRadius,
                        };
                      } else if (i === profiles.length - 1) {
                        style = {
                          borderBottomLeftRadius: borderRadius,
                          borderBottomRightRadius: borderRadius,
                        };
                      }
                      return (
                        <ListItem
                          disablePadding
                          key={p.name}
                          divider={i < profiles.length - 1}
                        >
                          <ListItemButton
                            sx={style}
                            onClick={() => toggleProfile(p)}
                            selected={search.includes(p)}
                          >
                            <ListItemAvatar>
                              <p.icon />
                            </ListItemAvatar>
                            <ListItemText
                              primary={p.name}
                              secondary={p.subtitle}
                            />
                          </ListItemButton>
                        </ListItem>
                      );
                    })}
                  </List>
                </Paper>
              </Grid>
            </Grid>
            <Grid item lg={6} xs={12} container direction="column" spacing={2}>
              <Grid item>
                <Typography variant="h4" textAlign="center">
                  Skills
                </Typography>
              </Grid>
              <Grid item>
                <Autocomplete
                  fullWidth
                  multiple
                  filterSelectedOptions
                  freeSolo
                  options={profiles}
                  getOptionLabel={o => (typeof o === 'string' ? o : o.name)}
                  value={search}
                  onChange={(e, v) => setState(ps => ({ ...ps, search: v }))}
                  renderInput={params => (
                    <TextField {...params} variant="outlined" />
                  )}
                />
              </Grid>
              <Grid item container spacing={2}>
                {list.length > 0 ? (
                  list.map(i => (
                    <Grid key={i.name} item sm={3} xs={6}>
                      <i.component alwaysOpen />
                    </Grid>
                  ))
                ) : (
                  <Grid item xs={12}>
                    <Alert severity="error">
                      Sorry, no skill found for your search
                    </Alert>
                  </Grid>
                )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </Element>
  );
};

export default Competences;
