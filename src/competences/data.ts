import {
  DeveloperBoardRounded,
  DevicesRounded,
  DnsRounded,
  RocketLaunchRounded,
  SmartphoneRounded,
  SvgIconComponent,
  WebRounded,
} from '@mui/icons-material';

export interface Profile {
  icon: SvgIconComponent;
  name: string;
  subtitle: string;
  tag: ProfileTags;
}

export enum ProfileTags {
  front,
  back,
  mobile,
  iot,
  devops,
  emm,
}

export const profiles: Profile[] = [
  {
    icon: WebRounded,
    name: 'Front-end developer',
    subtitle: 'Angular, React, RxJS, ...',
    tag: ProfileTags.front,
  },
  {
    icon: DnsRounded,
    name: 'Back-end developer',
    subtitle: 'ASP.NET, Springboot, NodeJS, ...',
    tag: ProfileTags.back,
  },
  {
    icon: SmartphoneRounded,
    name: 'Mobile developer',
    subtitle: 'Flutter, Android, ...',
    tag: ProfileTags.mobile,
  },
  {
    icon: DeveloperBoardRounded,
    name: 'IoT developer',
    subtitle: 'RTOS, GPIO, ...',
    tag: ProfileTags.iot,
  },
  {
    icon: RocketLaunchRounded,
    name: 'DevOps consultant',
    subtitle: 'Gitlab CI, Azure DevOps, Teamcity, ...',
    tag: ProfileTags.devops,
  },
  {
    icon: DevicesRounded,
    name: 'EMM support engineer',
    subtitle: 'AirWatch, MobileIron, ...',
    tag: ProfileTags.emm,
  },
];
