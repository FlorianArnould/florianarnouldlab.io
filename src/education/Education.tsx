import React, { FC } from 'react';
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Container,
  Grid,
  Typography,
} from '@mui/material';
import data from './data';
import { Element } from 'react-scroll';
import { Ids } from '../ids';
import { Link } from '@mui/icons-material';
import styles from './Education.module.css';

const Education: FC = () => {
  return (
    <Element name={Ids.education}>
      <Container>
        <Grid container rowSpacing={10} direction="column" alignItems="center">
          <Grid item>
            <Typography variant="h2">Education</Typography>
          </Grid>
          <Grid item container spacing={2}>
            {data.map(e => (
              <Grid item md={4} sm={12} xs={12} key={e.school}>
                <Card variant="outlined">
                  <CardActionArea onClick={() => window.open(e.link, '_blank')}>
                    <CardMedia component="img" height={140} image={e.photo} />
                    <CardContent>
                      <Typography variant="h6">{e.school}</Typography>
                      <Typography variant="body2" color="text.secondary">
                        {e.diploma}
                      </Typography>
                      <Typography variant="h6">{e.dates}</Typography>
                      <div className={`linkIcon ${styles.linkIcon}`}>
                        <Link />
                      </div>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Container>
    </Element>
  );
};

export default Education;
