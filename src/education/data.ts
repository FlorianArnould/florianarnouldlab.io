import EnsicaenPhoto from '../assets/images/ensicaen-photo.png';
import IaeCaenPhoto from '../assets/images/iae-caen-photo.png';
import UnicaenPhoto from '../assets/images/unicaen-photo.png';

export interface Education {
  school: string;
  diploma: string;
  dates: string;
  link: string;
  photo: string;
}

const data: Education[] = [
  {
    school: 'ENSICAEN',
    diploma: 'French engineering school',
    dates: '2015-2018',
    photo: EnsicaenPhoto,
    link: 'https://www.ensicaen.fr/en/formations/computer-science-engineer/',
  },
  {
    school: 'IAE Caen',
    diploma: 'Master of Business Administration',
    dates: '2017-2018',
    photo: IaeCaenPhoto,
    link: 'https://www.iae.unicaen.fr/formations-fiche.php?id_diplome=19',
  },
  {
    school: 'UNICAEN',
    diploma: 'IT Security Master',
    dates: '2018',
    photo: UnicaenPhoto,
    link: 'https://www.info.unicaen.fr/master/info/esecure',
  },
];

export default data;
