import React, { FC, useState } from 'react';
import {
  Card,
  CardActionArea,
  CardContent,
  CardHeader,
  Chip,
  Collapse,
  Grid,
  IconButton,
  IconButtonProps,
  styled,
  Typography,
} from '@mui/material';
import { ExpandMoreRounded, Link } from '@mui/icons-material';
import { Experience } from './data';
import SkillRows from './SkillRows';
import styles from './ExperienceCard.module.css';

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

interface State {
  expanded: boolean;
}

interface Props {
  experience: Experience;
}

const formatter = new Intl.DateTimeFormat('en-BE', {
  year: 'numeric',
  month: 'long',
});

const ExperienceCard: FC<Props> = ({ experience }) => {
  const [{ expanded }, setState] = useState<State>({ expanded: false });
  const header = (
    <CardHeader
      avatar={<experience.icon />}
      title={experience.name}
      subheader={`${formatter.format(experience.start)} - ${
        experience.end ? formatter.format(experience.end) : 'Now'
      }`}
      classes={{ action: styles.actionContainer }}
      action={
        <div className={styles.actions}>
          {!experience.company?.link ? (
            <Chip label={experience.company?.name ?? experience.type} />
          ) : (
            <Chip
              component="a"
              label={experience.company.name}
              href={experience.company.link}
              target="_blank"
              clickable
            />
          )}
          <div className={`linkIcon ${styles.linkIcon}`}>
            <Link />
          </div>
        </div>
      }
    />
  );
  return (
    <Card variant="outlined">
      {!experience.link ? (
        header
      ) : (
        <CardActionArea
          disabled={!experience.link}
          onClick={() => window.open(experience.link, '_blank')}
        >
          {header}
        </CardActionArea>
      )}
      <CardContent>
        <Typography variant="body2" color="text.secondary" paragraph>
          {experience.shortDescription}
        </Typography>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          {experience.description.map((d, i) => (
            <Typography
              key={i}
              variant="body2"
              color="text.secondary"
              paragraph
            >
              <span dangerouslySetInnerHTML={{ __html: d }} />
            </Typography>
          ))}
        </Collapse>
        <Grid container justifyContent="center">
          <ExpandMore
            expand={expanded}
            onClick={() => setState(ps => ({ ...ps, expanded: !expanded }))}
          >
            <ExpandMoreRounded />
          </ExpandMore>
        </Grid>
        <SkillRows skills={experience.skills} />
      </CardContent>
    </Card>
  );
};

export default ExperienceCard;
