import React, {
  ChangeEvent,
  FC,
  useCallback,
  useEffect,
  useState,
} from 'react';
import {
  Alert,
  Button,
  Container,
  Grid,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from '@mui/material';
import { CloseRounded, SearchRounded } from '@mui/icons-material';
import ExperienceCard from './ExperienceCard';
import data, { Experience, keys } from './data';
import Fuse from 'fuse.js';
import { Ids } from '../ids';
import { Element } from 'react-scroll';

interface State {
  search: string;
  filtered: Experience[];
  size: number;
}

const fuse = new Fuse(data, {
  includeScore: true,
  shouldSort: true,
  keys: keys,
});

const Experiences: FC = () => {
  const [{ search, filtered, size }, setState] = useState<State>({
    search: '',
    filtered: data,
    size: 4,
  });
  const more = useCallback(
    () => setState(ps => ({ ...ps, size: ps.size + 4 })),
    [],
  );
  const handleSearch = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    setState(ps => ({ ...ps, search: event.target.value }));
  }, []);
  const onClear = useCallback(
    () => setState(ps => ({ ...ps, search: '' })),
    [],
  );
  useEffect(() => {
    const array =
      search.length === 0
        ? data
        : fuse
            .search(search)
            .slice(0, size)
            .map(r => r.item);
    setState(ps => ({ ...ps, filtered: array }));
  }, [search]);
  return (
    <Element name={Ids.experiences}>
      <Container>
        <Grid container rowSpacing={10} direction="column" alignItems="stretch">
          <Grid item>
            <Typography variant="h2" textAlign="center">
              Experience
            </Typography>
          </Grid>
          <Grid item container rowSpacing={2} direction="column">
            <Grid item>
              <TextField
                variant="outlined"
                value={search}
                onChange={handleSearch}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchRounded />
                    </InputAdornment>
                  ),
                  endAdornment:
                    search.length === 0 ? null : (
                      <InputAdornment position="end">
                        <IconButton onClick={onClear}>
                          <CloseRounded />
                        </IconButton>
                      </InputAdornment>
                    ),
                }}
                style={{ width: '100%' }}
              />
            </Grid>
            <Grid item container spacing={2}>
              {filtered.length > 0 ? (
                filtered.slice(0, size).map(e => (
                  <Grid key={e.id} item md={6} sm={12} xs={12}>
                    <ExperienceCard experience={e} />
                  </Grid>
                ))
              ) : (
                <Grid item xs={12}>
                  <Alert severity="error">
                    Sorry, no experience found for your search
                  </Alert>
                </Grid>
              )}
            </Grid>
            {filtered.length <= size ? null : (
              <Grid item container justifyContent="center">
                <Button variant="outlined" onClick={more}>
                  More
                </Button>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Container>
    </Element>
  );
};

export default Experiences;
