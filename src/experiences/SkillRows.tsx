import { Icon } from '../shared/icons/Icons';
import { Grid, useMediaQuery } from '@mui/material';
import React, { FC } from 'react';

interface Props {
  skills: Icon[];
}

const SkillRows: FC<Props> = ({ skills }) => {
  const needs2Lines = useMediaQuery('(max-width:450px)') && skills.length > 4;
  const cutSkills = needs2Lines
    ? [
        skills.slice(0, Math.ceil(skills.length / 2)),
        skills.slice(Math.floor(skills.length / 2) + 1, skills.length),
      ]
    : [skills];
  return (
    <>
      {cutSkills.map((skills, i) => (
        <Grid
          key={i}
          container
          justifyContent="space-around"
          marginTop={1}
          flexWrap="nowrap"
        >
          {skills.map(i => {
            return (
              <Grid
                key={i.name}
                item
                flex={1}
                container
                justifyContent="center"
              >
                <i.component />
              </Grid>
            );
          })}
        </Grid>
      ))}
    </>
  );
};

export default SkillRows;
