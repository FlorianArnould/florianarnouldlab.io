import {
  AirWatch,
  Android,
  Angular,
  ASPNETCore,
  AzureDevOps,
  ChromeOS,
  Cypress,
  Docker,
  Elixir,
  ESP32,
  Flutter,
  GitlabCI,
  Gradle,
  GraphQL,
  Icon,
  IOS,
  Java,
  Jenkins,
  Kotlin,
  Maven,
  MobileIron,
  NodeJs,
  OpenApi,
  Phoenix,
  Php,
  PostgreSQL,
  Protobuf,
  Python,
  ReactIcon,
  Redux,
  RxJs,
  Spark,
  SpringBoot,
  TeamCity,
  Travis,
  Windows10,
} from '../shared/icons/Icons';
import { FC } from 'react';
import {
  ArianeProjectLogo,
  AwEuropeLogo,
  BeeBusyLogo,
  BoschLogo,
  EnsicaenLogo,
  EvsLogo,
  LyraNetworkLogo,
  NalysLogo,
  OrangeLogo,
  SiemensLogo,
  TecLogo,
  ThirstyPlantsLogo,
} from '../shared/logos/Logos';

export interface Experience {
  id: string;
  company?: { name: string; link?: string };
  name: string;
  start: Date;
  end: Date | null;
  shortDescription: string;
  description: string[];
  skills: Icon[];
  type: 'Personal' | 'Professional' | 'Student' | 'Intern';
  icon: FC;
  link?: string;
}

export const keys: string[] = [
  'company',
  'name',
  // 'start',
  // 'end',
  'shortDescription',
  'description',
  'skills.name',
  'type',
];

let id = 1;

const experiences: Experience[] = [
  {
    id: (id++).toString(),
    type: 'Personal',
    icon: BeeBusyLogo,
    company: { name: 'MUG Agency', link: 'https://www.mug-agency.com' },
    name: 'Beesy',
    start: new Date(2023, 10),
    end: null,
    shortDescription: `An application ot connect all your calendars from 
    different sources like Microsoft, Google, etc and sync them together. The 
    final goal is to be able to correctly show your availability in all of 
    these.`,
    description: [`All of this is a work in progress.`],
    skills: [Elixir, Phoenix],
  },
  {
    id: (id++).toString(),
    type: 'Professional',
    icon: ArianeProjectLogo,
    company: { name: 'Ariane Project' },
    name: 'Software engineer',
    start: new Date(2023, 3),
    end: null,
    shortDescription: `Recruited to work on a B2C mortgage credit platform for
    the Belgian market. Also, helping with some tasks on a .NET Framework
    application for the B2B market`,
    description: [
      `This company work on a lot of different projects in fields with
      a kind of synergy.`,
      `The main activity is a communication service for the real estate agencies
      around the properties they are selling. That's the one showcased on the 
      company <a href="https://arianeproject.be" target="_blank">website</a>.`,
    ],
    skills: [ASPNETCore, Angular, AzureDevOps, Cypress],
  },
  {
    id: (id++).toString(),
    type: 'Personal',
    icon: ThirstyPlantsLogo,
    name: 'Thirsty plants',
    start: new Date(2020, 5),
    end: null,
    shortDescription: `The goal is to create a mobile application to monitor 
    your home plants. This is based on two part for the end user, a mobile 
    application and an IoT device to put in the pot.`,
    description: [
      `All of this is a work in progress.`,
      `The mobile application is based on flutter and is able to interact with 
      the backend, displaying information or receiving notification from it 
      (Firebase). This application is also used to configure the IoT device, it 
      works almost like a 'Chromecast', you configure your Wi-Fi and calibrate 
      some stuff using a BLE connection.`,
      `On the hardware aspect, the IoT device is based on the ESP32-SOLO-1 and 
      some other sensors to measure the soil moisture, air temperature, and air 
      humidity. This software part is done using the espressif RTOS SDK and C 
      programming language.`,
      `Finally the backend, based on ASP.NET and running in docker, it serves 
      the data to the mobile application but also receive the information from 
      the IoT devices and process it.`,
    ],
    skills: [ASPNETCore, Flutter, ESP32, OpenApi, Protobuf, Docker, GitlabCI],
    link: 'https://thirsty-plants.be',
  },
  {
    id: (id++).toString(),
    type: 'Professional',
    icon: EvsLogo,
    company: { name: 'EVS' },
    name: 'Full Stack developer',
    start: new Date(2021, 9),
    end: new Date(2023, 3),
    shortDescription: `Helping a SCRUM team to speed up the development of a 
    chrome / electron video editing tool. Improving a timeline, video player and
    some other parts of the application.`,
    description: [
      `EVS Broadcast Equipment SA is a Belgian company that manufactures live 
      outside broadcast digital video production systems. Their XT-VIA 
      production video servers enable the creation, editing, exchange and 
      playout of audio and video feeds.`,
      `The Editing team needed help to ramp up the development speed and 
      deliver their 
      <a 
      href="https://evs.com/company/news/evs-launches-ipd-via-create"
      target="_blank">
      IPD-VIA Create</a> product in time.`,
      `I was involved in the development of multiple parts of the application 
      including the React / hyperHTML frontend, some backends based on either 
      ASP.NET or SpringBoot.`,
    ],
    skills: [
      ReactIcon,
      Redux,
      ASPNETCore,
      SpringBoot,
      Docker,
      Cypress,
      TeamCity,
    ],
  },
  {
    id: (id++).toString(),
    type: 'Professional',
    icon: TecLogo,
    company: { name: 'TEC' },
    name: 'Full Stack DevOps developer',
    start: new Date(2020, 3),
    end: new Date(2021, 8),
    shortDescription: `Development of a project management web application 
    using the client technological stack. Including a CI/CD, some end-to-end
    tests and a docker deployment in Kubernetes.`,
    description: [
      `Opérateur de transport de Wallonie (OTW) (Transport Operator of 
      Wallonia), formerly Société Régionale Wallonne du Transport (SRWT) 
      (Walloon Regional Transport Company), is responsible for the supervision, 
      strategic planning and marketing of a group of five regional public 
      transport directorate branded as TEC or "Transport En Commun" (Public 
      Transport) in Wallonia, Belgium. It is primarily a bus operator, but also 
      operates the Charleroi tram system. TEC was founded in 1991 through the 
      breakup of the former Belgian SNCV into separate companies for Wallonia 
      and Flanders.`,
      `The company is used to manage project using a custom excel form handling
      all the project management fields needed. They wanted to improve this way
      to work implementing a project management web app. The idea was to provide
      a centralized source of project information.`,
    ],
    skills: [Angular, ASPNETCore, OpenApi, Cypress, AzureDevOps, Docker],
  },
  {
    id: (id++).toString(),
    type: 'Professional',
    icon: AwEuropeLogo,
    company: { name: 'AW Europe' },
    name: 'Full Stack DevOps developer',
    start: new Date(2019, 11),
    end: new Date(2020, 3),
    shortDescription: `Some months to develop a webapp with 2 main features : 
      create a custom process (using drag & drop) and follow some serial 
      numbers through it via multiple databases and notifying the key users 
      in case of problem.`,
    description: [
      `AW Europe and AWTC Europe are the respective business 
      and R&D wings that drive the operations of the AISIN AW group in 
      Europe. Partner to most of the major car manufacturers over the 
      world, AISIN AW is the world's leading specialist supplier of 
      Automatic Transmissions and Navigation Systems technology. They used 
      to provide a really high quality on their product, and so they 
      uselly process a lot of Quality mission to fix, adapt and/or modify 
      their products to match the customer’s needs.`,
      `The tracking of the products having to follow these processes was 
      done manually through different IT systems and even sometimes on 
      some Excel sheets, that's why they wanted to have a web tracking 
      tool displaying a status of the serial number in these processes.`,
      `Later in this mission, to help getting faster in their developments, 
      they also needed an authentication / authorization micro-service. 
      This approach also helped them to get SSO on their web applications.`,
    ],
    skills: [ReactIcon, ASPNETCore, OpenApi, Cypress, Python, GitlabCI, Docker],
  },
  {
    id: (id++).toString(),
    type: 'Professional',
    icon: SiemensLogo,
    company: { name: 'Siemens' },
    name: 'Full Stack coach developer',
    start: new Date(2019, 8),
    end: new Date(2019, 10),
    shortDescription: `We started with a POC creating a Angular / NodeJS 
      application featuring unit / integration and e2e tests running in a 
      docker CI.`,
    description: [
      `Siemens Digital Industries Software is a division in 
      the Siemens group working on simulation softwares like Simcenter 3D. 
      In an environment of web growing, they want to take on the 
      transition to cloud platforms with their Finite Elements Analysis 
      software. They don’t want to cannibalize their existing desktop 
      software, but rather access new markets where usage of the 
      traditional desktop software could not be justified, either because 
      of license costor learning curve.`,
      `In this context, there were 2 projects, the first one in Leuven 
      working with a worldwide team working on a 3D simulation solution 
      and the second one based in Lyon (France) working on multidomain 
      dynamical systems simulation.`,
      `The beginning was a training in the Brussels Nalys headquarters 
      preparing some demo compiling all our knowledge about their 
      technologies and coaching other Nalys consultant to ramp up on the 
      web technologies. At the end of the training the mission started on 
      site in Leuven. My time was shared between the 2 projects developing 
      and still coaching the Nalys consultants who were new to the web 
      technologies.`,
    ],
    skills: [Angular, NodeJs, GraphQL, Cypress, GitlabCI, Docker],
  },
  {
    id: (id++).toString(),
    type: 'Professional',
    icon: NalysLogo,
    company: { name: 'Nalys' },
    name: 'Full Stack DevOps developer',
    start: new Date(2019, 0),
    end: new Date(2019, 7),
    shortDescription: `Some ERP features development through multiple 
    micro-services to integrate with a custom internal database.`,
    description: [
      `Nalys is a consulting group dedicated to high-technology projects.`,
      `In order to improve some data management in their company they 
      created a group of web applications named Jeffrey. This 
      micro-service ecosystem host multiple features related to document, 
      holidays, timesheets, skills management, sickness leave, ...`,
      `At the beginning, the work was mainly about the holidays application 
      to improve the Front-End part and fix some bugs to drive it to its 
      first production release.`,
      `Once it was done, we developed some other micro-services like 
      timesheets, absences, expenses, ...`,
    ],
    skills: [
      Angular,
      SpringBoot,
      ASPNETCore,
      Kotlin,
      OpenApi,
      GitlabCI,
      Docker,
    ],
  },
  {
    id: (id++).toString(),
    type: 'Professional',
    name: 'Webapp graduate',
    icon: NalysLogo,
    company: { name: 'Nalys' },
    start: new Date(2018, 9),
    end: new Date(2018, 11),
    shortDescription: `A 10 weeks web app training, organised by the 
      Nalys Institute of Technology to teach Angular, RxJs and Springboot
      basics to the trainees.`,
    description: [
      `In order to create better than regular junior
      engineers, the Nalys Institute of Technology (NIT) organizes the 
      Graduate Program: a hands-on industrial grade training program. 
      During the first 7 weeks of the program, the engineers are taught in 
      hard and soft skills. This training is organized in theory and lab 
      sessions, with an emphasis on practical workshops. It teaches the 
      engineers to work under pressure and at a high pace. After the first 
      7 weeks of trainings, a 3-weeks Graduation Project is organized in 
      which a real-life problem is solved together with a client. After 
      the Graduate Program, the engineers are coached by experienced Nalys 
      engineers.`,
    ],
    skills: [Angular, SpringBoot, Java(8), RxJs, Jenkins, GitlabCI, Docker],
  },
  {
    id: (id++).toString(),
    type: 'Intern',
    icon: OrangeLogo,
    name: 'EMM Support engineer',
    start: new Date(2018, 2),
    end: new Date(2018, 7),
    shortDescription: `Technological watch over new technological solutions 
    in the EMM field and support provider for different clients`,
    description: [
      `Final internship in the French company Orange Connectivity 
      & Workspaces services as an Enterprise Mobility Management Support 
      Engineer in the Digital Mobility Management team. This part of the 
      company sells some B2B services to other companies and the team helps 
      them to solve their problem or make consultancy about the integration 
      and the deployment of these solutions.`,
    ],
    skills: [AirWatch, MobileIron, Windows10, ChromeOS, Android, IOS],
  },
  {
    id: (id++).toString(),
    type: 'Student',
    icon: LyraNetworkLogo,
    name: 'E-Banking developer',
    start: new Date(2017, 9),
    end: new Date(2018, 1),
    shortDescription: `POC development implementing the new European PS2 
      Payment Chain designed by STET.`,
    description: [
      `Last year project at the ENSICAEN French engineering 
      school. This project was done in collaboration with Lyra Network. This 
      project lasted 5 months with the objective of developing a POC of the 
      new PS2 Payment Chain designed by STET and the European Union. We were 
      two students working on a specific payment scenario.`,
    ],
    skills: [Java(8), Spark, Travis],
  },
  {
    id: (id++).toString(),
    type: 'Intern',
    icon: BoschLogo,
    name: 'Java intern',
    start: new Date(2017, 4),
    end: new Date(2017, 6),
    shortDescription: `Internship featuring java development covering some
      internal needs for the other development teams at Bosch.`,
    description: [
      `Bosch is a big actor in the field of the ESP and ABS software’s.`,
      `The CC/ESM5 Team in the Product Line Tool department 
      develops some internal software tools deploys them.`,
      `The company uses the Rational Team Concert Version Control System from 
      IBM and they work with the API provided for it. This one is complex and 
      with a high level of abstraction. This is the reason why they develop 
      some tools and libraries to simplify the use of this API. The work during 
      these three months turned around this API in relation with multiple 
      projects.`,
    ],
    skills: [Java(8), Maven, Jenkins],
  },
  {
    id: (id++).toString(),
    type: 'Student',
    icon: EnsicaenLogo,
    name: 'Android / PHP Full Stack developer',
    start: new Date(2016, 9),
    end: new Date(2017, 2),
    shortDescription: `A 6 months development, to get a student application
      covering some club organisation use-cases.`,
    description: [
      `Semester project during the second year of engineering 
      school at the ENSICAEN. The project was done with a group of two other 
      students. The subject was our personal idea to enhance the community 
      life of our school under the supervision of a teacher.`,
    ],
    skills: [Android, Java(7), Gradle, Php, PostgreSQL],
  },
];

export default experiences;
