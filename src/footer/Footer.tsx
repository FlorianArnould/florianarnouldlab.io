import React, { FC } from 'react';
import {
  Container,
  Grid,
  IconButton,
  Link,
  List,
  ListItem,
  ListItemButton,
  Paper,
  ThemeProvider,
  Typography,
  useTheme,
} from '@mui/material';
import { darkTheme } from '../themes';
import linkedIn from '@iconify/icons-simple-icons/linkedin';
import github from '@iconify/icons-simple-icons/github';
import gitlab from '@iconify-icons/ph/gitlab-logo-simple-fill';
import { InlineIcon } from '@iconify/react';
import { ReactComponent as LicenseLogo } from '../assets/images/wtfpl.svg';
import { ExpandLessRounded } from '@mui/icons-material';
import { Link as ScrollLink } from 'react-scroll';
import { Ids } from '../ids';

const links = [
  { name: 'Home', id: Ids.home },
  { name: 'Experiences', id: Ids.experiences },
  { name: 'Education', id: Ids.education },
  { name: 'Competences', id: Ids.competences },
  { name: 'Languages', id: Ids.languages },
];

const Footer: FC = () => {
  const theme = useTheme();
  return (
    <ThemeProvider theme={darkTheme}>
      <Paper elevation={0} square>
        <Container>
          <Grid
            container
            spacing={4}
            padding={theme.spacing(1, 12, 12, 12)}
            justifyContent="space-between"
            flexWrap="wrap"
          >
            <Grid
              item
              xs={12}
              container
              justifyContent="center"
              paddingBottom={theme.spacing(6)}
            >
              <Grid item>
                <ScrollLink to={Ids.home} smooth>
                  <IconButton>
                    <ExpandLessRounded />
                  </IconButton>
                </ScrollLink>
              </Grid>
            </Grid>
            <Grid item>
              <Grid container spacing={4}>
                <Grid item>
                  <Link
                    href="https://www.linkedin.com/in/florian-arnould-175145151"
                    target="_blank"
                  >
                    <InlineIcon icon={linkedIn} width={20} />
                  </Link>
                </Grid>
                <Grid item>
                  <Link
                    href="https://github.com/FlorianArnould?tab=repositories"
                    color="primary"
                    target="_blank"
                  >
                    <InlineIcon icon={github} width={20} />
                  </Link>
                </Grid>
                <Grid item>
                  <Link
                    href="https://gitlab.com/FlorianArnould"
                    color="primary"
                    target="_blank"
                  >
                    <InlineIcon icon={gitlab} width={20} />
                  </Link>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Link
                href="https://gitlab.com/FlorianArnould/florianarnould.gitlab.io/-/blob/master/LICENSE"
                color="inherit"
                underline="none"
                target="_blank"
              >
                <Grid container spacing={3} alignItems="center">
                  <Grid item>
                    <LicenseLogo height={20} />
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" color="text.secondary">
                      WTFPL license - {new Date().getFullYear()}
                    </Typography>
                  </Grid>
                </Grid>
              </Link>
            </Grid>
            <Grid item>
              <List>
                {links.map(l => (
                  <ScrollLink key={l.id} to={l.id} smooth>
                    <ListItem disablePadding>
                      <ListItemButton
                        style={{ borderRadius: theme.shape.borderRadius }}
                      >
                        <Typography variant="body2" color="text.secondary">
                          {l.name}
                        </Typography>
                      </ListItemButton>
                    </ListItem>
                  </ScrollLink>
                ))}
              </List>
            </Grid>
          </Grid>
        </Container>
      </Paper>
    </ThemeProvider>
  );
};

export default Footer;
