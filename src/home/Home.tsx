import React, { FC } from 'react';
import { Player } from '@lottiefiles/react-lottie-player';
import {
  Container,
  IconButton,
  Paper,
  styled,
  ThemeProvider,
  Typography,
  useTheme,
} from '@mui/material';
import animation from '../assets/animation.json';
import { lightTheme } from '../themes';
import { ExpandMoreRounded } from '@mui/icons-material';
import { Ids } from '../ids';
import { Element, Link } from 'react-scroll';

const Root = styled('div')(() => ({
  display: 'flex',
  position: 'relative',
  height: '100vh',
  width: '100%',
  alignItems: 'center',
  justifyContent: 'center',
}));

const Center = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  [theme.breakpoints.down('md')]: {
    flexWrap: 'wrap-reverse',
  },
}));

const BottomCenter = styled('div')(({ theme }) => ({
  position: 'absolute',
  bottom: theme.spacing(3),
}));

const Home: FC = () => {
  const theme = useTheme();
  return (
    <Element name={Ids.home}>
      <Container>
        <Root>
          <Center>
            <ThemeProvider theme={lightTheme}>
              <Paper elevation={0} style={{ margin: theme.spacing(2) }}>
                <Player
                  autoplay
                  loop
                  src={animation}
                  style={{
                    maxWidth: '700px',
                  }}
                />
              </Paper>
            </ThemeProvider>
            <div style={{ margin: theme.spacing(2) }}>
              <Typography variant="h1">Florian Arnould</Typography>
              <Typography variant="h4" fontFamily="jr-hand">
                Yet another fullstack developer
              </Typography>
            </div>
          </Center>
          <BottomCenter>
            <Link to={Ids.experiences} smooth>
              <IconButton>
                <ExpandMoreRounded />
              </IconButton>
            </Link>
          </BottomCenter>
        </Root>
      </Container>
    </Element>
  );
};

export default Home;
