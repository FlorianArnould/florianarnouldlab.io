export enum Ids {
  home = 'home',
  experiences = 'experiences',
  education = 'education',
  competences = 'competences',
  languages = 'languages',
}
