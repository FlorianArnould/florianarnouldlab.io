import React, { FC } from 'react';
import { Card, CardContent, Container, Grid, Typography } from '@mui/material';
import languages from './data';
import { Element } from 'react-scroll';
import { Ids } from '../ids';

const md = 12 / languages.length;
const isOdd = languages.length % 2 !== 0;

const Languages: FC = () => {
  return (
    <Element name={Ids.languages}>
      <Container>
        <Grid container direction="column" rowSpacing={10}>
          <Grid item>
            <Typography variant="h2" textAlign="center">
              Languages
            </Typography>
          </Grid>
          <Grid item>
            <Card variant="outlined">
              <CardContent>
                <Grid container>
                  {languages.map((l, i) => (
                    <Grid
                      key={l.name}
                      item
                      md={md}
                      xs={isOdd && i + 1 === languages.length ? 12 : 6}
                      container
                      padding={2}
                      direction="column"
                      alignItems="center"
                    >
                      <Grid item>
                        <Typography variant="h5">{l.name}</Typography>
                      </Grid>
                      <Grid item>
                        <Typography variant="body2" color="text.secondary">
                          {l.description}
                        </Typography>
                      </Grid>
                    </Grid>
                  ))}
                </Grid>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </Element>
  );
};

export default Languages;
