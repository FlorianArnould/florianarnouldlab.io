export interface Language {
  name: string;
  description: string;
}

const languages: Language[] = [
  { name: 'French', description: 'Mother tongue' },
  { name: 'English', description: '840 TOEIC' },
  { name: 'German', description: 'Intermediate level' },
  { name: 'Portuguese', description: 'Notions' },
  { name: 'Dutch', description: 'Beginner' },
];

export default languages;
