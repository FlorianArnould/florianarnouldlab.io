import './Chip.css';
import React, { FC, PropsWithChildren } from 'react';
import { css } from '@emotion/css';
import { useTheme } from '@mui/material';

interface Props {
  name: string;
  color?: string;
  big?: boolean;
  alwaysOpen?: boolean;
}

const Chip: FC<PropsWithChildren<Props>> = ({
  name,
  color,
  big,
  alwaysOpen,
  children,
}) => {
  const coloring = color
    ? css`
        :hover > *:first-child {
          color: ${color};
        }
      `
    : 'coloring-logo';
  const justColor = css`
    > *:first-child {
      color: ${color};
    }
  `;
  const theme = useTheme();
  return (
    <div
      className={`skill-chip 
        ${alwaysOpen ? justColor : coloring} 
        ${big ? 'big-chip' : ''} 
        ${alwaysOpen ? '' : 'wrapped'}
        ${theme.palette.mode === 'light' ? '' : 'dark'}
      `}
    >
      {children}
      <div className="chip-text">
        <span className="inner-chip-text">{name}</span>
      </div>
    </div>
  );
};

export default Chip;
