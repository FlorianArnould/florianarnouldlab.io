import bxlAndroid from '@iconify/icons-bx/bxl-android';
import javaIcon from '@iconify/icons-bx/bxl-java';
import angularIcon from '@iconify/icons-logos/angular-icon';
import bootstrapLogo from '@iconify/icons-logos/bootstrap';
import cLogo from '@iconify/icons-logos/c';
import cppLogo from '@iconify/icons-logos/c-plusplus';
import chromeLogo from '@iconify/icons-logos/chrome';
import dockerLogo from '@iconify/icons-logos/docker-icon';
import flutterIcon from '@iconify/icons-logos/flutter';
import gitlabLogo from '@iconify/icons-logos/gitlab';
import graphQlLogo from '@iconify/icons-logos/graphql';
import jsLogo from '@iconify/icons-logos/javascript';
import kotlinLogo from '@iconify/icons-logos/kotlin-icon';
import windowsLogo from '@iconify/icons-logos/microsoft-windows-icon';
import nodeJsLogo from '@iconify/icons-logos/nodejs-icon';
import phoenixIcon from '@iconify/icons-logos/phoenix';
import pythonLogo from '@iconify/icons-logos/python';
import qtLogo from '@iconify/icons-logos/qt';
import reactLogo from '@iconify/icons-logos/react';
import reactivexLogo from '@iconify/icons-logos/reactivex';
import reduxLogo from '@iconify/icons-logos/redux';
import sparkLogo from '@iconify/icons-logos/spark';
import teamCityIcon from '@iconify/icons-logos/teamcity';
import mavenIcon from '@iconify/icons-simple-icons/apachemaven';
import azureDevOps from '@iconify/icons-simple-icons/azuredevops';
import cypressLogo from '@iconify/icons-simple-icons/cypress';
import dotNetIcon from '@iconify/icons-simple-icons/dotnet';
import elixirIcon from '@iconify/icons-simple-icons/elixir';
import espressifIcon from '@iconify/icons-simple-icons/espressif';
import bashIcon from '@iconify/icons-simple-icons/gnubash';
import gradleIcon from '@iconify/icons-simple-icons/gradle';
import iOSIcon from '@iconify/icons-simple-icons/ios';
import jenkinsIcon from '@iconify/icons-simple-icons/jenkins';
import openApiIcon from '@iconify/icons-simple-icons/openapiinitiative';
import phpIcon from '@iconify/icons-simple-icons/php';
import postgresIcon from '@iconify/icons-simple-icons/postgresql';
import sonarQubeIcon from '@iconify/icons-simple-icons/sonarqube';
import springIcon from '@iconify/icons-simple-icons/spring';
import travisIcon from '@iconify/icons-simple-icons/travisci';
import cSharpLogo from '@iconify/icons-vscode-icons/file-type-csharp2';
import protobufLogo from '@iconify/icons-vscode-icons/file-type-protobuf';
import { IconifyIcon, InlineIcon } from '@iconify/react';
import React, { FC } from 'react';
import airWatchIcon from '../../assets/images/AirWatch.png';
import mobileIronIcon from '../../assets/images/MobileIron.png';
import { ProfileTags } from '../../competences/data';
import Chip from '../chip/Chip';
import './Icons.css';

export interface IconProps {
  big?: boolean;
  alwaysOpen?: boolean;
}

export interface Icon {
  name: string;
  component: FC<IconProps>;
  tags: ProfileTags[];
}

function chipFactory(
  name: string,
  icon: string | IconifyIcon,
  tags: ProfileTags[],
  options?: {
    color?: string;
    height?: number;
    bigHeight?: number;
    width?: number;
  },
): Icon {
  return {
    name,
    tags,
    component: ({ big, alwaysOpen }: IconProps) => (
      <Chip
        name={name}
        color={options?.color}
        alwaysOpen={alwaysOpen}
        big={big}
      >
        <InlineIcon
          icon={icon}
          height={big ? options?.bigHeight : options?.height}
          width={options?.width}
        />
      </Chip>
    ),
  };
}

export const Android = chipFactory(
  'Android',
  bxlAndroid,
  [ProfileTags.mobile, ProfileTags.emm],
  { color: '#A4C439' },
);

export const AzureDevOps = chipFactory(
  'Azure DevOps',
  azureDevOps,
  [ProfileTags.devops],
  {
    color: '#0089d6',
    height: 24,
  },
);

export const Php = chipFactory(
  'PHP',
  phpIcon,
  [ProfileTags.back, ProfileTags.front],
  { color: '#6181b6' },
);

export const Java: (version?: number) => Icon = (version?: number) => ({
  name: `Java${version || ''}`,
  tags: [ProfileTags.mobile, ProfileTags.back],
  component: ({ big, alwaysOpen }: IconProps) => (
    <Chip
      name={`Java${version ? ` ${version}` : ''}`}
      color="#EA2D2E"
      big={big}
      alwaysOpen={alwaysOpen}
    >
      <InlineIcon icon={javaIcon} height={big ? 40 : 28} />
    </Chip>
  ),
});

export const PostgreSQL = chipFactory(
  'PostgreSQL',
  postgresIcon,
  [ProfileTags.back],
  {
    color: '#336791',
    height: 24,
  },
);

export const Gradle = chipFactory(
  'Gradle',
  gradleIcon,
  [ProfileTags.mobile, ProfileTags.back],
  {
    color: '#02303a',
    height: 28,
  },
);

export const Maven = chipFactory('Maven', mavenIcon, [ProfileTags.back], {
  color: '#FF5700',
  height: 24,
  bigHeight: 40,
});

export const Jenkins = chipFactory(
  'Jenkins',
  jenkinsIcon,
  [ProfileTags.devops],
  {
    color: '#E4001A',
    height: 24,
    bigHeight: 40,
  },
);

export const Spark = chipFactory('Java spark', sparkLogo, [ProfileTags.back], {
  height: 24,
});

export const Travis = chipFactory('Travis', travisIcon, [ProfileTags.devops], {
  color: '#02303a',
  height: 24,
  bigHeight: 40,
});

export const Angular = chipFactory(
  'Angular',
  angularIcon,
  [ProfileTags.front],
  {
    height: 24,
    bigHeight: 40,
  },
);

export const ChromeOS = chipFactory(
  'Chrome OS',
  chromeLogo,
  [ProfileTags.emm],
  { height: 24 },
);

export const Windows10 = chipFactory(
  'Windows 10',
  windowsLogo,
  [ProfileTags.emm],
  { height: 24 },
);

export const MobileIron: Icon = {
  name: 'MobileIron',
  tags: [ProfileTags.emm],
  component: ({ big, alwaysOpen }: IconProps) => (
    <Chip name="MobileIron" alwaysOpen={alwaysOpen} big={big}>
      <img className="height-24" src={mobileIronIcon} alt="MobileIron logo" />
    </Chip>
  ),
};

export const AirWatch: Icon = {
  name: 'AirWatch',
  tags: [ProfileTags.emm],
  component: ({ big, alwaysOpen }: IconProps) => (
    <Chip name="AirWatch" big={big} alwaysOpen={alwaysOpen}>
      <img className="height-24" src={airWatchIcon} alt="AirWatch logo" />
    </Chip>
  ),
};

export const IOS = chipFactory('iOS', iOSIcon, [ProfileTags.emm], {
  color: 'black',
  width: 32,
});

export const SpringBoot = chipFactory(
  'Spring Boot',
  springIcon,
  [ProfileTags.back],
  {
    color: '#3FC53B',
    height: 24,
    bigHeight: 38,
  },
);

export const GitlabCI = chipFactory(
  'Gitlab CI',
  gitlabLogo,
  [ProfileTags.devops],
  { height: 24 },
);

export const Docker = chipFactory('Docker', dockerLogo, [ProfileTags.devops], {
  height: 24,
  bigHeight: 38,
});

export const ASPNETCore = chipFactory(
  'ASP.NET Core',
  dotNetIcon,
  [ProfileTags.back],
  { color: '#02303a' },
);

export const ReactIcon = chipFactory(
  'React.js',
  reactLogo,
  [ProfileTags.front],
  {
    height: 24,
    bigHeight: 40,
  },
);

export const Redux = chipFactory('Redux', reduxLogo, [ProfileTags.front], {
  height: 24,
  bigHeight: 40,
});

export const OpenApi = chipFactory(
  'OpenAPI',
  openApiIcon,
  [ProfileTags.front, ProfileTags.back],
  {
    color: '#02303a',
    height: 24,
    bigHeight: 40,
  },
);

export const Cypress = chipFactory(
  'Cypress',
  cypressLogo,
  [ProfileTags.front],
  {
    color: '#5c5c5e',
    height: 24,
    bigHeight: 40,
  },
);

export const Python = chipFactory(
  'Python',
  pythonLogo,
  [ProfileTags.back, ProfileTags.iot],
  {
    height: 24,
    bigHeight: 40,
  },
);

export const RxJs = chipFactory('RxJS', reactivexLogo, [ProfileTags.front], {
  height: 24,
  bigHeight: 40,
});

export const NodeJs = chipFactory('Node.js', nodeJsLogo, [ProfileTags.back], {
  height: 24,
});

export const GraphQL = chipFactory(
  'GraphQL',
  graphQlLogo,
  [ProfileTags.front, ProfileTags.back],
  { height: 24 },
);

export const Kotlin = chipFactory(
  'Kotlin',
  kotlinLogo,
  [ProfileTags.back, ProfileTags.mobile],
  {
    height: 20,
    bigHeight: 32,
  },
);

export const C = chipFactory('C', cLogo, [ProfileTags.iot], {
  height: 24,
  bigHeight: 40,
});

export const Cpp = chipFactory('C++', cppLogo, [ProfileTags.back], {
  height: 24,
  bigHeight: 40,
});

export const CSharp = chipFactory('C#', cSharpLogo, [ProfileTags.back], {
  height: 28,
  bigHeight: 40,
});

export const Bash = chipFactory('Bash', bashIcon, [ProfileTags.devops], {
  height: 24,
  bigHeight: 40,
});

export const JavaScript = chipFactory(
  'JavaScript',
  jsLogo,
  [ProfileTags.front, ProfileTags.back],
  {
    height: 24,
    bigHeight: 32,
  },
);

export const Bootstrap = chipFactory(
  'Bootstrap',
  bootstrapLogo,
  [ProfileTags.front],
  {
    width: 22,
    bigHeight: 32,
  },
);

export const Qt = chipFactory('Qt', qtLogo, [ProfileTags.front], {
  width: 24,
  bigHeight: 32,
});

export const SonarQube = chipFactory(
  'SonarQube',
  sonarQubeIcon,
  [ProfileTags.devops],
  {
    color: '#4A90D9',
    height: 22,
    bigHeight: 35,
  },
);

export const Protobuf = chipFactory(
  'Protobuf',
  protobufLogo,
  [ProfileTags.front, ProfileTags.back, ProfileTags.mobile, ProfileTags.iot],
  {
    height: 24,
    bigHeight: 40,
  },
);

export const TeamCity = chipFactory(
  'TeamCity',
  teamCityIcon,
  [ProfileTags.devops],
  {
    width: 24,
    bigHeight: 32,
  },
);

export const Flutter = chipFactory(
  'Flutter',
  flutterIcon,
  [ProfileTags.mobile],
  {
    height: 24,
    bigHeight: 40,
  },
);

export const ESP32 = chipFactory('ESP32', espressifIcon, [ProfileTags.iot], {
  color: '#E7352C',
  height: 24,
  bigHeight: 40,
});

export const Elixir = chipFactory(
  'Elixir',
  elixirIcon,
  [ProfileTags.back, ProfileTags.front],
  {
    color: '#553a67',
    height: 24,
    bigHeight: 40,
  },
);

export const Phoenix = chipFactory(
  'Phoenix',
  phoenixIcon,
  [ProfileTags.back, ProfileTags.front],
  {
    height: 24,
    bigHeight: 40,
  },
);

export const allIcons = [
  Java(8),
  Android,
  AzureDevOps,
  Php,
  PostgreSQL,
  Gradle,
  Maven,
  Jenkins,
  Spark,
  Travis,
  Angular,
  ChromeOS,
  Windows10,
  MobileIron,
  AirWatch,
  IOS,
  SpringBoot,
  GitlabCI,
  Docker,
  ASPNETCore,
  ReactIcon,
  OpenApi,
  Cypress,
  Python,
  RxJs,
  NodeJs,
  GraphQL,
  Kotlin,
  C,
  Cpp,
  CSharp,
  Bash,
  JavaScript,
  Bootstrap,
  Qt,
  SonarQube,
  Redux,
  Protobuf,
  TeamCity,
  Flutter,
  ESP32,
  Elixir,
  Phoenix,
];
allIcons.sort((a, b) => a.name.localeCompare(b.name));
