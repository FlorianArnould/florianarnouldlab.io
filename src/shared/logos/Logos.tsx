import { lightTheme } from '../../themes';
import { Paper, ThemeProvider } from '@mui/material';
import { ReactComponent as ThirstyPlants } from '../../assets/images/thirsty-plants.svg';
import Nalys from '../../assets/images/nalys.png';
import { ReactComponent as Siemens } from '../../assets/images/siemens.svg';
import { ReactComponent as Bosch } from '../../assets/images/bosch.svg';
import { ReactComponent as Ensicaen } from '../../assets/images/ensicaen.svg';
import { ReactComponent as LyraNetwork } from '../../assets/images/lyra.svg';
import { ReactComponent as Tec } from '../../assets/images/tec.svg';
import { ReactComponent as Evs } from '../../assets/images/evs.svg';
import { ReactComponent as Orange } from '../../assets/images/orange.svg';
import { ReactComponent as ArianeProject } from '../../assets/images/ArianeProject.svg';
import React, { FC } from 'react';
import AwEurope from '../../assets/images/aw-europe.png';
import BeeBusy from '../../assets/images/beebusy.png';
import styles from './Logos.module.css';

export const ThirstyPlantsLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper variant="outlined">
        <ThirstyPlants />
      </Paper>
    </ThemeProvider>
  );
};

export const NalysLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper variant="outlined" className={styles.container}>
        <img alt="Nalys logo" src={Nalys} className={styles.icon} />
      </Paper>
    </ThemeProvider>
  );
};

export const AwEuropeLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper
        variant="outlined"
        className={styles.container}
        style={{ backgroundColor: '#fefefe' }}
      >
        <img alt="AW Europe logo" src={AwEurope} style={{ width: 48 }} />
      </Paper>
    </ThemeProvider>
  );
};

export const SiemensLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper variant="outlined" className={styles.container}>
        <Siemens className={styles.icon} />
      </Paper>
    </ThemeProvider>
  );
};

export const BoschLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper variant="outlined" className={styles.container}>
        <Bosch className={styles.icon} />
      </Paper>
    </ThemeProvider>
  );
};

export const EnsicaenLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper variant="outlined" className={styles.container}>
        <Ensicaen className={styles.icon} />
      </Paper>
    </ThemeProvider>
  );
};

export const LyraNetworkLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper variant="outlined" className={styles.container}>
        <LyraNetwork className={styles.icon} />
      </Paper>
    </ThemeProvider>
  );
};

export const TecLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper variant="outlined" className={styles.container}>
        <Tec className={styles.icon} />
      </Paper>
    </ThemeProvider>
  );
};

export const EvsLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper variant="outlined" className={styles.container}>
        <Evs className={styles.icon} />
      </Paper>
    </ThemeProvider>
  );
};

export const OrangeLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper variant="outlined" className={styles.container}>
        <Orange className={styles.icon} />
      </Paper>
    </ThemeProvider>
  );
};

export const BeeBusyLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper variant="outlined" className={styles.container}>
        <img alt="BeeBusy logo" src={BeeBusy} style={{ width: 48 }} />
      </Paper>
    </ThemeProvider>
  );
};

export const ArianeProjectLogo: FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <Paper variant="outlined" className={styles.container}>
        <ArianeProject className={styles.icon} />
      </Paper>
    </ThemeProvider>
  );
};
