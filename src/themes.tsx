import React from 'react';
import { createTheme, Theme } from '@mui/material';
import { grey, indigo, orange } from '@mui/material/colors';

interface LocalColors {
  primary: string;
  secondary: string;
  background: string;
}

const light: LocalColors = {
  primary: indigo[900],
  secondary: orange[200],
  background: grey[50],
};

const dark: LocalColors = {
  primary: indigo[500],
  secondary: orange[300],
  background: grey[900],
};

function themeFactory(colors: LocalColors): Theme {
  return createTheme({
    palette: {
      mode: colors === light ? 'light' : 'dark',
      primary: {
        main: colors.primary,
      },
      secondary: {
        main: colors.secondary,
      },
      background: {
        paper: colors.background,
        default: colors.background,
      },
    },
    shape: {
      borderRadius: 20,
    },
    typography: {
      h1: {
        fontFamily: 'Roboto',
        fontWeight: 900,
      },
      h2: {
        fontFamily: 'Roboto',
        fontWeight: 700,
        '@media (max-width: 450px)': {
          fontSize: '3rem',
        },
      },
    },
    components: {
      MuiTab: {
        styleOverrides: {
          root: {
            minWidth: '120px',
          },
        },
      },
      MuiTabs: {
        defaultProps: {
          TabIndicatorProps: {
            children: (
              <span
                style={{
                  maxWidth: 40,
                  width: '100%',
                  backgroundColor: colors.primary,
                }}
              />
            ),
          },
        },
        styleOverrides: {
          indicator: {
            display: 'flex',
            justifyContent: 'center',
            backgroundColor: 'transparent',
          },
        },
      },
    },
  });
}

export const lightTheme = themeFactory(light);
export const darkTheme = themeFactory(dark);
